# The script of the game goes in this file.
init python:
    ###Screenshake
    # Shakes the screen. To use, put
    # $ shake()
    # inline. For other uses, simply do a check inline for ATL statements, or a ConditionSwitch for shaky images.

    def shake():
        if persistent.screenshake:
            renpy.with_statement(hpunch)
        else:
            renpy.with_statement(fade) ###OPTIONAL: Show a different effect if screenshake is turned off.

# Set up LayeredImage Sprites
layeredimage jane:
    group face auto:
        attribute left_smile default


image rain:
        "images/sprites/rain1.png"
        0.1
        "images/sprites/rain2.png"
        0.1
        "images/sprites/rain3.png"
        0.1
        repeat

transform centered:
        xalign 0.5
        yalign 0.7

transform depie:
        xalign 0.5
        yoffset -160

transform veryleft:
        xoffset -200

image monkey1:
    "images/sprites/monkey1_base1.png"
image monkey1 moves:
        "images/sprites/monkey1_base1.png"
        0.3
        "images/sprites/monkey1_base2.png"
        0.3
        "images/sprites/monkey1_base3.png"
        0.3
        repeat

image grooming:
        "images/sprites/grooming_1.png"
        0.3
        "images/sprites/grooming_2.png"
        0.3
        "images/sprites/grooming_3.png"
        0.3
        "images/sprites/grooming_1.png"
        0.3
        "images/sprites/grooming_2.png"
        0.3
        "images/sprites/grooming_3.png"
        0.3
        "images/sprites/grooming_4.png"
        0.3
        "images/sprites/grooming_5.png"
        0.3
        "images/sprites/grooming_1.png"
        0.3
        "images/sprites/grooming_2.png"
        0.3
        "images/sprites/grooming_3.png"
        0.3
        "images/sprites/grooming_1.png"
        0.3
        "images/sprites/grooming_2.png"
        0.3
        "images/sprites/grooming_3.png"
        0.3
        "images/sprites/grooming_4.png"
        0.3
        "images/sprites/grooming_6.png"
        0.3
        repeat

image glups:
        "images/sprites/glups-1.png"
        0.8
        "images/sprites/glups-2.png"
        0.8
        "images/sprites/glups-3.png"
        0.8
        "images/sprites/glups-4.png"
        0.7
        "images/sprites/glups-5.png"
        0.7
        "images/sprites/glups-6.png"
        0.7
        "images/sprites/glups-7.png"
        0.8
        "images/sprites/glups-8.png"
        0.8
        "images/sprites/glups-9.png"
        0.8
        "images/sprites/glups-10.png"
    #    1.5
    #    repeat

image glups loop:
        "images/sprites/glups-9.png"
        0.8
        "images/sprites/glups-10.png"
        0.8
        repeat


image claclac:
    "images/sprites/claclac-1.png"
image claclac moves:
        "images/sprites/claclac-1.png"
        1
        "images/sprites/claclac-2.png"
        0.2
        "images/sprites/claclac-3.png"
        0.2
        "images/sprites/claclac-2.png"
        0.2
        "images/sprites/claclac-3.png"
        0.2
        "images/sprites/claclac-2.png"
        0.2
        "images/sprites/claclac-3.png"
        0.2
        repeat

image claclacb moves:
        "images/sprites/claclacb-1.png"
        1
        "images/sprites/claclacb-2.png"
        0.2
        "images/sprites/claclacb-3.png"
        0.2
        "images/sprites/claclacb-2.png"
        0.2
        "images/sprites/claclacb-3.png"
        0.2
        "images/sprites/claclacb-2.png"
        0.2
        "images/sprites/claclacb-3.png"
        0.2
        repeat

image parenting:
        "images/sprites/parenting_1.png"
        0.4
        "images/sprites/parenting_2.png"
        0.4
        "images/sprites/parenting_1.png"
        0.4
        "images/sprites/parenting_2.png"
        0.4
        "images/sprites/parenting_1.png"
        0.4
        "images/sprites/parenting_2.png"
        0.4
        "images/sprites/parenting_1.png"
        0.4
        "images/sprites/parenting_2.png"
        0.4
        "images/sprites/parenting_3.png"
        0.2
        "images/sprites/parenting_4.png"
        0.2
        "images/sprites/parenting_5.png"
        0.4
        repeat

image palo rotando:
        "images/sprites/palo_1.png"
        0.2
        "images/sprites/palo_2.png"
        0.2
        "images/sprites/palo_3.png"
        0.2
        "images/sprites/palo_4.png"
        0.2
        "images/sprites/palo_5.png"
        0.2
        "images/sprites/palo_6.png"
        0.2
        "images/sprites/palo_7.png"
        0.2
        "images/sprites/palo_8.png"
        0.2
        repeat

# Declare characters used by this game. The color argument colorizes the
# name of the character.

define p = Character("[name]")
define e = Character("Jane", color="#8ae6ff", bold=True)
define n = Character(None, what_italic=True)
# define ic = Character(None, what_color="#8ae6ff")
$ persistent.sound_captions = True
$ persistent.image_captions = True

## Splashscreen ############################################################
## A portion of the game that plays at launch, before the main menu is shown.

## The animation is kinda tacky so I recommend using something else.
## ATL documentation: https://www.renpy.org/doc/html/atl.html

image splash_anim_1:

    "gui/renpy-logo.png"
    xalign 0.5 yalign 0.5 alpha 0.0
    ease_quad 7.0 alpha 1.0 zoom 1.5

label splashscreen:

    scene black

    ## The first time the game is launched, players can set their accessibility settings.
#    if not persistent.caption:
#
#        menu:
#
#            "¿Quieres descripción de los sonidos? Describen la música y los efectos de sonido en el texto.{fast}"
#
#            "Sí":
#
#                $ persistent.sound_captions = True
#
#            "No":
#
#                pass
#
#        menu:
#
#            "¿Quieres descripción de las imágenes? {fast}"
#
#            "Sí":
#
#                $ persistent.image_captions = True
#
#            "No":
#
#                pass
#
#        "Puedes cambiar estas opciones siempre que quieras en el menú.  También el tipo de fuente, tamaño, velocidad del texto...{fast}"
#
        ## This message will not appear in subsequent launches of the game when
        ## the following variable becomes true.
#        $ persistent.caption = True

    ## Here begins our splashscreen animation.
    show splash_anim_1
    show text "{size=60}Hecho con amor y Ren'Py [renpy.version_only]{/s}":
        xalign 0.5 yalign 0.8 alpha 0.0
        pause 6.0
        linear 1.0 alpha 1.0

    ## The first time the game is launched, players cannot skip the animation.
    if not persistent.seen_splash:

        ## No input will be detected for the set time stated.
        ## Set this to be a little longer than how long the animation takes.
        $ renpy.pause(8.5, hard=True)

        $ persistent.seen_splash = True

    ## Players can skip the animation in subsequent launches of the game.
    else:

        if renpy.pause(8.5):

            jump skip_splash

    scene black
    with fade

    label skip_splash:

        pass

    return

## The game starts here.

label start:

    # Show a background. This uses a placeholder by default, but you can
    # add a file (named either "bg room.png" or "bg room.jpg") to the
    # images directory to show it.

    scene bg_entrada

#    $ renpy.unlink_save("quitsave")
#    $ _quit_slot = None


    # Para hacer scroll de dcha a izda
    # scene panoramicacartel:
    #    linear 4.0 xalign 1.0


    # This plays our music file in a way that if audio captions are on,
    # it will tell us the name of the song. This music plays at full volume
    # after 2 seconds and fades out after 2 seconds when stopped.
    #$ play_music(garden,fadein=2.0,fadeout=2.0)

    # This unlocks the the achievement with the corresponding name
    #$ achievement.grant("Beginning")

    # This adds an integer value to a point-based achievement.
    # To track how much of it has been earned, use a regular variable for now.
    #$ achievement.progress("Point_Collector", 10)
    #$ persistent.points =+ 10

    # These display lines of dialogue.

    $play_music(forest)

    "En las PREFERENCIAS, puedes cambiar las opciones de accesibilidad: descripciones, tamaño de letra, volumen del sonido, etcétera."

    menu:

        "TUTORIAL":

            ## This empty label is solely for replay mode purposes.

            label tutorial:

                pass

            "¡Hola! Vamos a hacer un repaso rápido a cómo se juega. Para avanzar en los diálogos pulsa espacio, ENTER o haz clic con el ratón donde quieras."
            "Si pulsas V activas y desactivas el voice-over."
            "Si seleccionas \"AUTO\", los textos avanzarán solos."
            "Si pulsas H haces aparecer y desaparecer los textos y ver la imagen."
            "Si pulsas F entras y sales de pantalla completa."
            "Durante tu aventura tendrás opciones de diálogo como las siguientes. Pulsa en una para elegirla."

            menu:

                "Opción 1":

                    ## This empty label is solely for replay mode purposes.

                    label tuto1:

                        pass

                    "Has elegido la opción 1."

                "Opción2":

                    label tuto2:

                        pass

                    "Has elegido la opción 2."

            "Muy bien."

            "Pulsando \"ATRÁS\" o usando la rueda del ratón puedes moverte por las conversaciones que ya has tenido."
            "¡Así puedes volver a leer algo o elegir opciones distintas!"

            "En \"AYUDA\" puedes ver estas y otras opciones."

            "Ahora, ¡vamos a jugar!"





        "JUGAR":

            label jugar:

                pass



    ic "Se ve una panorámica del refugio. Un arco de entrada donde pone MonkeyWorld"

    n "Qué ilusión me hace poder venir a ver monetes. ¡Tenía muchas ganas!"
    n "Voy a visitar un sitio donde cuidan a monos rescatados. ¡Monkeyworld ha de ser lo más!"


    # This shows a character sprite.

    ic "Aparece Jane"

    show jane left_comein at depie:



    e "¡Hola! Soy Jane, la directora de este refugio de monos."

#    $ _quit_slot = "quitsave"


    # This changes the sprite's expression

    show jane left_talk with dissolve

    e "¿Cómo te llamas?"

    python:
        name = renpy.input("¿Cómo te llamas?")
        name = name.strip() or "Colega"


    e "Encantada de conocerte, [name]. ¡Te damos la bienvenida a MonkeyWorld!"

    menu:

        "Hola, Jane. ¿Eres científica?":

            ## This empty label is solely for replay mode purposes.

            label cientifica:

                pass

            e "Sí, me dedico a investigar cosas sobre un tipo de monos, los primates. Soy primatóloga. Los macacos son mis monos preferidos."

            # $ achievement.grant("Office")

            # $ play_music(business,fadein=2.0,fadeout=2.0)

            # scene future_office
            # show jane angry at center:
            #    yoffset 250
            # with fade


        "¡Qué chulo! ¿Cuáles son tus monos preferidos?":

            label macacos:

                pass

            e "Me dedico a investigar sobre todo un tipo de monos que se llaman macacos."

            # $ achievement.grant("Beach")

            # $ play_music(summer,fadein=2.0,fadeout=2.0)

            # scene sort_of_beautiful_beach_day
            # show jane upset at center:
            #     yoffset 250
            # with fade



    "¿Maca… qué?"

    show jane left_comein at center:

    e "Macacos, son varias especies de monos que viven en muchos sitios, algunos en Gibraltar. Son muy curiosos. ¿Te los presento?"

    "¡Claro! ¡Qué pasada!"


    scene playground

    $play_sound(monagibraltar)

    ic "Se ve un patio con estructuras de madera. En una de ellas hay una rueda colgada de cuerdas"

    n "Ese es el columpio más raro que he visto en mi vida. ¿Para qué será?"

    show jane big_left_neutrex at veryleft:

    e "Aquí es donde viven los macacos. Pero ahora no sé dónde están… ¿Dónde se habrán metido?"

    show grooming:
        zoom 0.6
        xalign 0.5
        yalign 0.6

    show claclac:
        zoom 0.35
        xalign 0.98
        yalign 0.03

    ic "Se ven tres macacos. Dos están más cerca, en el suelo. Uno está limpiando al otro, quitándole cosas de entre el pelo"
    ic "El otro está más lejos, subido en una estructura"

    show jane big_left_comein at veryleft:

    ic "Jane señala a los que están en el suelo"

    e "¡Ah! Ahí está Andy acicalando a Núria. [name], ¿sabes por qué se acicalan los macacos?"

    show jane big_left_smile at veryleft:

    menu:

        "Para quitarse parásitos, como pulgas":

            ## This empty label is solely for replay mode purposes.

            label parasitos:

                pass


        "Para hacerse más amigos":

            label amigos:

                pass


    e "Jejeje. Era pregunta trampa. Es para las dos cosas. Es algo que hacen muchos monos."

    ic "La imagen se acerca a la macaco que está subida en la estructura"


    scene playground:
        zoom 2
        xalign 1.0
        # linear 2.0 zoom 2
        # repeat

    show claclac moves:
        zoom 0.8
        xalign 1.0
        yalign 0.01

    # show claclac moves:
    #    zoom 0.7
    #    zoom 0.35
    #    xalign 1.0
    #    yalign 0.03
    #    linear 2.0 xalign 1.0
    #    linear 2.0 yalign 0.06
    #    linear 2.0 zoom 0.7

    e "Ahí subida puedes ver a Sara. Es la jefa de la manada."

    ic "Sara está mirándote enseñando los dientes y abriendo y cerrando la boca, chasqueándolos"

    menu:

        "¿La jefa?":

            ## This empty label is solely for replay mode purposes.

            label jefa:

                pass

            e "Sí. En los macacos siempre hay una macaca que es la jefa. Aquí ahora es Sara, que es muy mandona."
            e "Antes la jefa era Núria, que era mucho más tranquila. Con ella vivían mucho mejor."

        "¿Por qué hace eso con la boca?":

            label boca:

                pass

            e "Es una señal de saludo y como diciendo que está todo bien. Suelen hacerlo a los peques y a los macacos de mayor nivel."
            e "Pero también nos saludan a las cuidadoras así."



    menu:

        "¿En los grupos de monos ellas son siempre las jefas?":

            ## This empty label is solely for replay mode purposes.

            label todasjefas:

                pass

            e "No siempre, depende de la especie del mono. Entre los macacos sí."
            e "Y todo el mundo, papás y mamás, cuidan a las crías. Las macacas tratan mejor a los macacos que más cuidan a los macacos bebes."



        "¿Siempre van en grupos?":

            label grupos:

                pass

            e "Sí, les gusta vivir en grupo, como a las personas. Necesitan estar con los de su especie, sobre todo al crecer."
            e "Por ejemplo, Andy creció solo y cuando vino no sabía ser amigo con los otros macacos."
            e "Tenía problemas porque hacía sin querer gestos que enfadaban a los demás. Pero ha aprendido mucho y ya no pasa."


    scene playground:

    show grooming:
        zoom 0.6
        xalign 0.5
        yalign 0.6

    show claclac moves:
        zoom 0.35
        xalign 1.0
        yalign 0.03

    show jane big_left_smile at veryleft:

    $ play_sound(monagibraltar)

    ic "Se vuelve a ver la imagen anterior completa con Jane, los dos monos acicalándose y otra subida al columpio"

    # $ shake()

    show palo rotando at right:
        zoom 0.7
        yalign 0.2
        linear 2.5 xoffset -2000


    show jane big_left_dodge at veryleft:
        yoffset 300
    ic "Un palo cruza la pantalla y la primatóloga se agacha"

    e "¡OH!"

    ic "Jane Sonríe"

    show jane big_left_talk at veryleft:
        yoffset 0

    e "Ese es Fer. Le encanta lanzar palos."

    scene playground:
        zoom 1.6
        yalign 0.7

    show claclacb moves:
        xalign 0.8
        yalign 0.7


    ic "Primer plano del macaco Fer. Fer saluda chasqueando los dientes"

    e "Ya ves que cada uno tiene su personalidad propia. Como las personas."

    "¿Por qué está aquí?"

    show jane big_left_sad at veryleft:

    e "Lo encontraron atado a una barandilla. Tenía unos seis meses."
    e "No sabemos dónde estaba antes."

    show jane big_left_neutrex at veryleft:

    e "Cuando llegó estaba muy nervioso y tenía problemas en adaptarse a sus compis."

    show jane big_left_smile at veryleft:

    e "Hoy está mucho mejor."

    e "¡Vaya! No me había dado cuenta de la hora. Es la hora de comer. [name], me ayudas?"

    menu:

        "Me dijeron que no hay que dar de comer a los animales salvajes":

            ## This empty label is solely for replay mode purposes.

            label nocomer:

                pass

            e "Y te dijeron muy bien. Aquí es porque es un refugio y les damos solo la comida que les sienta bien."



        "¡Sí!":

            label sicomer:

                pass

            e "Pero esto lo hacemos aquí porque es un refugio y les damos solo la comida que les sienta bien."
            e "Recuerda que a los animales salvajes no hay que darles comida."
            "Para que no se acostumbren y porque podemos darle cosas que les siente mal, ¿no?"
            e "Eso es. Muy bien."

    e "Hablando de eso. ¿Sabes qué comen?"

    menu:

        "Comen carne. Son carnívoros.":

            ## This empty label is solely for replay mode purposes.

            label carnivoros:

                pass

            e "Pues no, aunque sí que comen insectos. Pero en general son hervívoros."



        "Comen plantas y fruta. Son hervívoros.":

            label hervivoros:

                pass

            e "Eso es. Zanahorias, hierba, fruta... Aunque también les gusta comer insectos."
            "¡Jajaja! entonces son igual que mis gatos."
            ic "Jane ríe"

            e "La verdad es que un poco sí."


    ic "aparece una cesta con zanahorias, hierba, fruta..."

    e "Venga, vamos a darles la comida."

    hide grooming
    hide claclacb
    hide jane

    show glups:
        yalign 0.4
        xalign 0.5

    ic "Se ve a un macaco eligiendo comida y cogiéndola. Le van saliendo unos bultos en el cuello"

    show glups loop:
        yalign 0.4
        xalign 0.5


    menu:

        "¿Qué son esos bultos en el cuello?":

            ## This empty label is solely for replay mode purposes.

            label bultos:

                pass



        "Mira, ese está malito":

            label malito:

                pass


    e "No te preocupes, es normal. Los macacos tienen una especie de bolsas en la boca donde pueden guardar comida."

    "¿Para qué?"

    e "A veces para comerla más tarde, otras para coger todo lo que puedan rápido e ir a comerlo a un sitio más tranquilo."

    scene playground

    show jane left_sad at depie:
    show rain:
        zoom 2

    $ play_sound(thunder)
    ic "Empieza a llover. Los monos van a refugiarse. Jane está triste."

    e "Vaya, está lloviendo. Volvamos al centro de visitantes. Allí tenemos juegos sobre la selva."

    "CONTINUARÁ..."

    scene bg_entrada
    show jane big_left_smile

    ic "Volvemos a ver la entrada al centro. Jane está delante, sonriendo"

    "Si tienes sugerencias o quieres comentarnos algo, escribe a: monkeyworldgame@gmail.com"
